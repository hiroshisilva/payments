package com.payments.common.service;

import com.payments.TestConfig;
import com.payments.common.exception.BrandCardNotFoundException;
import com.payments.common.model.BrandCard;
import com.payments.common.repository.BrandCardRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class BrandCardServiceTest {

    @Autowired
    private BrandCardService brandCardService;

    @Autowired
    private BrandCardRepository brandCardRepository;

    @Test
    public void getBrandCardById() throws BrandCardNotFoundException {

        BrandCard masterCard = new BrandCard().setDescription("Master Card");
        brandCardRepository.save(masterCard);

        BrandCard brandCardResult = brandCardService.getBrandCard(masterCard.getId());

        assertThat(brandCardResult, is(equalTo(masterCard)));

    }

    @Test(expected = BrandCardNotFoundException.class)
    public void getBrandCardByIdNotFound() throws BrandCardNotFoundException {

        brandCardService.getBrandCard(new Random().nextLong());

    }

}
