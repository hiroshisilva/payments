package com.payments.common.service;
import com.payments.TestConfig;
import com.payments.common.model.User;
import com.payments.common.model.UserType;
import com.payments.common.repository.UserRepository;
import com.payments.common.exception.UserNotFoundException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Test(expected = UserNotFoundException.class)
    public void getUserTestUserNotFound() throws UserNotFoundException {

        User user = userService.getUser(new Random().nextLong());
    }

    @Test
    public void getUserTest() throws UserNotFoundException {
        User user = new User()
                .setDocument("user-test")
                .setName("André-userTest")
                .setEmail("user.teste@gmail.com")
                .setTelephone("011982700817")
                .setType(UserType.NEW);

        userRepository.save(user);

        User userResult = userService.getUser(user.getId());

        assertThat(userResult,is(equalTo(user)));
    }

}
