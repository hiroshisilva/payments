package com.payments.common.service;


import com.payments.TestConfig;
import com.payments.common.exception.PaymentMethodNotFoundException;
import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentMethod;
import com.payments.common.repository.BrandCardRepository;
import com.payments.common.repository.PaymentMethodRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Random;

import static com.payments.common.model.PaymentMethodType.POS;
import static com.payments.common.model.PaymentOperationType.OFFLINE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class PaymentMethodServiceTest {

    @Autowired
    private PaymentMethodService paymentMethodService;

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Autowired
    private BrandCardRepository brandCardRepository;

    private BrandCard masterCard;

    private BrandCard visa;

    PaymentMethod paymentMethod;

    @Before
    public void before(){
        masterCard = new BrandCard().setDescription("Master Card");
        brandCardRepository.save(masterCard);

        visa = new BrandCard().setDescription("Visa");
        brandCardRepository.save(visa);

        paymentMethod = new PaymentMethod().addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Credito")
                .setOperationType(OFFLINE)
                .setType(POS);
        paymentMethodRepository.save(paymentMethod);
    }

    @After
    public void after(){
        paymentMethodRepository.delete(paymentMethod.getId());

        brandCardRepository.delete(visa.getId());
        brandCardRepository.delete(masterCard.getId());
    }

    @Test
    public void getPaymentMethod() throws PaymentMethodNotFoundException {

        PaymentMethod result = paymentMethodService.getPaymentMethod(paymentMethod.getId());

        assertThat(result, equalTo(paymentMethod));
    }

    @Test(expected = PaymentMethodNotFoundException.class)
    public void getPaymentMethodNotFound() throws PaymentMethodNotFoundException {
        PaymentMethod result = paymentMethodService.getPaymentMethod(new Random().nextLong());

    }

}
