package com.payments.common.service;

import com.payments.TestConfig;
import com.payments.common.model.Restaurant;
import com.payments.common.repository.RestaurantRepository;
import com.payments.common.exception.RestaurantNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;


import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class RestaurantServiceTest {

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Test
    public void getRestaurantById() throws RestaurantNotFoundException {

        Restaurant restaurant = new Restaurant()
                .setDocument("123452134321")
                .setName("Teste 45")
                .setTelephone("123432543");

        restaurantRepository.save(restaurant);

        Restaurant result = restaurantService.getRestaurant(restaurant.getId());


        assertThat(result, is(equalTo(restaurant)));
    }

    @Test(expected = RestaurantNotFoundException.class)
    public void getRestaurantByIdNotFound() throws RestaurantNotFoundException {

        Restaurant result = restaurantService.getRestaurant(new Random().nextLong());

    }

}
