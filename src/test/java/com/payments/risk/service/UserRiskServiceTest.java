package com.payments.risk.service;

import com.payments.TestConfig;
import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentMethod;
import com.payments.common.model.Restaurant;
import com.payments.common.model.User;
import com.payments.common.repository.BrandCardRepository;
import com.payments.common.repository.PaymentMethodRepository;
import com.payments.common.repository.RestaurantRepository;
import com.payments.common.repository.UserRepository;
import com.payments.common.exception.RestaurantNotFoundException;
import com.payments.common.exception.UserNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static com.payments.common.model.PaymentMethodType.CREDIT_CARD;
import static com.payments.common.model.PaymentMethodType.POS;
import static com.payments.common.model.PaymentOperationType.OFFLINE;
import static com.payments.common.model.PaymentOperationType.ONLINE;
import static com.payments.common.model.UserType.FRAUDSTER;
import static com.payments.common.model.UserType.RELIABLE;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class UserRiskServiceTest {

    @Autowired
    private UserRiskService userRiskService;

    @Autowired
    private BrandCardRepository brandCardRepository;

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void getPaymentMethodsTest() throws UserNotFoundException, RestaurantNotFoundException {

        BrandCard masterCard = new BrandCard().setDescription("Master Card");
        brandCardRepository.save(masterCard);

        BrandCard visa = new BrandCard().setDescription("Visa");
        brandCardRepository.save(visa);

        PaymentMethod debitPOS = new PaymentMethod()
                .addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Debito")
                .setOperationType(OFFLINE)
                .setType(POS);
        paymentMethodRepository.save(debitPOS);


        PaymentMethod creditPOS = new PaymentMethod().addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Credito")
                .setOperationType(OFFLINE)
                .setType(POS);
        paymentMethodRepository.save(creditPOS);

        PaymentMethod creditOnline = new PaymentMethod().addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Credito")
                .setOperationType(ONLINE)
                .setType(CREDIT_CARD);
        paymentMethodRepository.save(creditOnline);


        Restaurant restaurant = new Restaurant()
                .setDocument("123456789009")
                .setName("Testes - 1")
                .setTelephone("123432543")
                .addPaymentMethod(debitPOS)
                .addPaymentMethod(creditPOS)
                .addPaymentMethod(creditOnline);
        restaurant = restaurantRepository.save(restaurant);

        User user = new User().setDocument("12345678924309")
                .setName("Testes 1")
                .setEmail("hiroshisilva@gmail.com")
                .setTelephone("1234567891934")
                .setType(RELIABLE);
        user = userRepository.save(user);


        List<PaymentMethod> paymentMethods = userRiskService.getPaymentMethodsByUserIdRestaurantId(user.getId(), restaurant.getId());

        assertThat(paymentMethods.size(), equalTo(3));
        assertThat(paymentMethods, hasItems(creditPOS,creditOnline,debitPOS));

    }

    @Test
    public void getPaymentMethodsTestFraudster() throws UserNotFoundException, RestaurantNotFoundException {

        BrandCard masterCard = new BrandCard().setDescription("Master Card");
        brandCardRepository.save(masterCard);

        BrandCard visa = new BrandCard().setDescription("Visa");
        brandCardRepository.save(visa);

        PaymentMethod debitPOS = new PaymentMethod()
                .addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Debito")
                .setOperationType(OFFLINE)
                .setType(POS);
        paymentMethodRepository.save(debitPOS);


        PaymentMethod creditPOS = new PaymentMethod().addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Credito")
                .setOperationType(OFFLINE)
                .setType(POS);
        paymentMethodRepository.save(creditPOS);

        PaymentMethod creditOnline = new PaymentMethod().addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Credito")
                .setOperationType(ONLINE)
                .setType(CREDIT_CARD);
        paymentMethodRepository.save(creditOnline);


        Restaurant restaurant = new Restaurant()
                .setDocument("12345678900009")
                .setName("Testes - 1")
                .setTelephone("123432543")
                .addPaymentMethod(debitPOS)
                .addPaymentMethod(creditPOS)
                .addPaymentMethod(creditOnline);
        restaurant = restaurantRepository.save(restaurant);

        User user = new User().setDocument("12367678909")
                .setName("Testes 1")
                .setEmail("hiroshaaisilva@gmail.com")
                .setTelephone("12345678919")
                .setType(FRAUDSTER);
        user = userRepository.save(user);


        List<PaymentMethod> paymentMethods = userRiskService.getPaymentMethodsByUserIdRestaurantId(user.getId(), restaurant.getId());

        assertThat(paymentMethods.size(), equalTo(2));
        assertThat(paymentMethods, hasItems(creditPOS,debitPOS));

    }

}
