package com.payments.capture.service;

import com.payments.TestConfig;
import com.payments.capture.captor.CaptorFactory;
import com.payments.capture.dto.TransactionDTO;
import com.payments.capture.exception.CaptorNotImplementedException;
import com.payments.capture.exception.ProcessorNotAccessibleException;
import com.payments.capture.exception.ProcessorPriceNotFoundException;
import com.payments.capture.exception.UserCanNotUseThisOperationException;
import com.payments.capture.model.*;
import com.payments.capture.repository.ProcessorPriceRepository;
import com.payments.capture.repository.ProcessorRepository;
import com.payments.capture.repository.TransactionRepository;
import com.payments.common.exception.BrandCardNotFoundException;
import com.payments.common.exception.PaymentMethodNotFoundException;
import com.payments.common.exception.RestaurantNotFoundException;
import com.payments.common.exception.UserNotFoundException;
import com.payments.common.model.*;
import com.payments.common.repository.BrandCardRepository;
import com.payments.common.repository.PaymentMethodRepository;
import com.payments.common.repository.RestaurantRepository;
import com.payments.common.repository.UserRepository;
import com.payments.mock.dto.TransactionMockDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import static com.payments.common.model.PaymentMethodType.CREDIT_CARD;
import static com.payments.common.model.PaymentOperationType.ONLINE;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class TransactionServiceTest {


    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    @Mock
    private CaptorFactory captorFactory;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    @InjectMocks
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private BrandCardRepository brandCardRepository;

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Autowired
    private ProcessorRepository processorRepository;

    @Autowired
    private ProcessorPriceRepository processorPriceRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;
    

    private User user;

    private BrandCard visa;

    private Restaurant restaurant;

    private BrandCard masterCard;

    private PaymentMethod creditOnline;

    private ProcessorPrice processorPriceVisaCielo;

    private ProcessorPrice processorPriceMasterRede;

    private ProcessorPrice processorPriceMasterCielo;

    private ProcessorPrice processorPriceMasterPagseguro;



    @Before
    public void before(){

        MockitoAnnotations.initMocks(this);


        masterCard = new BrandCard().setDescription("Master Card");
        brandCardRepository.save(masterCard);

        visa = new BrandCard().setDescription("Visa");
        brandCardRepository.save(visa);

        creditOnline = new PaymentMethod()
                .addBrandCard(masterCard)
                .addBrandCard(visa)
                .setDescription("Credito")
                .setOperationType(ONLINE)
                .setType(CREDIT_CARD);
        paymentMethodRepository.save(creditOnline);

        restaurant = new Restaurant()
                .setDocument("testesEnqueue1")
                .setName("Testes - 1")
                .setTelephone("1234325243")
                .addPaymentMethod(creditOnline);
        restaurant = restaurantRepository.save(restaurant);

        user = new User()
                .setDocument("testesEnqueue1")
                .setName("André")
                .setEmail("andre.teste@gmail.com")
                .setTelephone("011982700817")
                .setType(UserType.RELIABLE);

        userRepository.save(user);


        processorRepository.save(Processor.REDE());
        processorRepository.save(Processor.CIELO());
        processorRepository.save(Processor.PAG_SEGURO());

        processorPriceVisaCielo = new ProcessorPrice()
                .setPrice(new BigDecimal(3.1))
                .setProcessor(Processor.CIELO())
                .setBrandCard(visa)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceVisaCielo);


        processorPriceMasterRede = new ProcessorPrice()
                .setPrice(new BigDecimal(3.0))
                .setProcessor(Processor.REDE())
                .setBrandCard(masterCard)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceMasterRede);

        processorPriceMasterCielo = new ProcessorPrice()
                .setPrice(new BigDecimal(4.0))
                .setProcessor(Processor.CIELO())
                .setBrandCard(masterCard)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceMasterCielo);


        processorPriceMasterPagseguro = new ProcessorPrice()
                .setPrice(new BigDecimal(5.0))
                .setProcessor(Processor.PAG_SEGURO())
                .setBrandCard(masterCard)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceMasterPagseguro);


    }


    @After
    public void after(){

        processorPriceRepository.delete(processorPriceMasterRede.getId());
        processorPriceRepository.delete(processorPriceMasterCielo.getId());
        processorPriceRepository.delete(processorPriceVisaCielo.getId());
        processorPriceRepository.delete(processorPriceMasterPagseguro.getId());

        restaurantRepository.delete(restaurant.getId());
        paymentMethodRepository.delete(creditOnline.getId());

        brandCardRepository.delete(visa.getId());
        brandCardRepository.delete(masterCard.getId());

        processorRepository.delete(Processor.REDE().getId());
        processorRepository.delete(Processor.CIELO().getId());
        processorRepository.delete(Processor.PAG_SEGURO().getId());

        userRepository.delete(user.getId());


    }


    @Ignore
    @Test
    public void enqueue() throws UserNotFoundException, BrandCardNotFoundException, RestaurantNotFoundException, PaymentMethodNotFoundException, UserCanNotUseThisOperationException, ProcessorPriceNotFoundException, CaptorNotImplementedException, ProcessorNotAccessibleException {


        TransactionDTO transactionDTO = new TransactionDTO()
                .setBrandCardId(visa.getId())
                .setCountry(Country.BR)
                .setCardNumber("123432432")
                .setCvv("020")
                .setValue(new BigDecimal(2.53))
                .setPaymentMethodId(creditOnline.getId())
                .setUserId(user.getId())
                .setRestaurantId(restaurant.getId());


        TransactionMockDTO transactionMockDTO =
                new TransactionMockDTO()
                        .setCredCard(transactionDTO.getCardNumber())
                        .setCvv(transactionDTO.getCvv())
                        .setValue(transactionDTO.getValue());

        ResponseEntity<TransactionMockDTO> responseEntity = new ResponseEntity<TransactionMockDTO>(transactionMockDTO, HttpStatus.OK);

        Mockito.when(restTemplate.postForEntity(Matchers.any(),Matchers.any(),eq(TransactionMockDTO.class))).thenReturn(responseEntity);

        Transaction transaction = transactionService.process(transactionDTO);

        assertThat(transaction, notNullValue());
        assertThat(transaction.getId(), notNullValue());

        transactionRepository.delete(transaction.getId());

    }

}
