package com.payments.capture.service;

import com.payments.TestConfig;
import com.payments.capture.exception.ProcessorPriceNotFoundException;
import com.payments.capture.model.Processor;
import com.payments.capture.model.ProcessorPrice;
import com.payments.capture.repository.ProcessorPriceRepository;
import com.payments.capture.repository.ProcessorRepository;
import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentOperationType;
import com.payments.common.repository.BrandCardRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class,loader = AnnotationConfigContextLoader.class)
public class ProcessorPriceServiceTest {

    @Autowired
    private ProcessorPriceService processorPriceService;

    @Autowired
    private ProcessorRepository processorRepository;

    @Autowired
    private BrandCardRepository brandCardRepository;

    @Autowired
    private ProcessorPriceRepository processorPriceRepository;

    private BrandCard visa;

    private BrandCard masterCard;

    private ProcessorPrice processorPriceVisaCielo;

    private ProcessorPrice processorPriceMasterRede;

    private ProcessorPrice processorPriceMasterCielo;

    private ProcessorPrice processorPriceMasterPagseguro;

    @Before
    public void before(){
        visa = new BrandCard().setDescription("Visa");
        brandCardRepository.save(visa);

        masterCard = new BrandCard().setDescription("Master Card");
        brandCardRepository.save(masterCard);

        processorRepository.save(Processor.REDE());
        processorRepository.save(Processor.CIELO());
        processorRepository.save(Processor.PAG_SEGURO());

        processorPriceVisaCielo = new ProcessorPrice()
                .setPrice(new BigDecimal(3.1))
                .setProcessor(Processor.CIELO())
                .setBrandCard(visa)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceVisaCielo);


        processorPriceMasterRede = new ProcessorPrice()
                .setPrice(new BigDecimal(3.0))
                .setProcessor(Processor.REDE())
                .setBrandCard(masterCard)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceMasterRede);

        processorPriceMasterCielo = new ProcessorPrice()
                .setPrice(new BigDecimal(4.0))
                .setProcessor(Processor.CIELO())
                .setBrandCard(masterCard)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceMasterCielo);


        processorPriceMasterPagseguro = new ProcessorPrice()
                .setPrice(new BigDecimal(5.0))
                .setProcessor(Processor.PAG_SEGURO())
                .setBrandCard(masterCard)
                .setOperationType(PaymentOperationType.ONLINE);
        processorPriceRepository.save(processorPriceMasterPagseguro);
    }

    @After
    public void after(){

        processorPriceRepository.delete(processorPriceMasterRede.getId());
        processorPriceRepository.delete(processorPriceMasterCielo.getId());
        processorPriceRepository.delete(processorPriceVisaCielo.getId());
        processorPriceRepository.delete(processorPriceMasterPagseguro.getId());

        brandCardRepository.delete(visa.getId());
        brandCardRepository.delete(masterCard.getId());

        processorRepository.delete(Processor.REDE().getId());
        processorRepository.delete(Processor.CIELO().getId());
        processorRepository.delete(Processor.PAG_SEGURO().getId());
    }

    @Test(expected = ProcessorPriceNotFoundException.class)
    public void findProcessorPriceNotFound() throws ProcessorPriceNotFoundException {
        processorPriceService.getProcessorByPrice(new BrandCard().setId(new Random().nextLong()), PaymentOperationType.OFFLINE, Processor.CIELO().getType(), Processor.CIELO().getUseAntiFraud());
    }

    @Test
    public void findProcessorPrice() throws ProcessorPriceNotFoundException {

        List<ProcessorPrice> result = processorPriceService.getProcessorByPrice(visa,PaymentOperationType.ONLINE, Processor.CIELO().getType(), Processor.CIELO().getUseAntiFraud());

        assertThat(result.size(), equalTo(1) );
    }

    @Test
    public void findProcessorPriceUseAntiFraud() throws ProcessorPriceNotFoundException {

        List<ProcessorPrice> result = processorPriceService.getProcessorByPrice(masterCard,PaymentOperationType.ONLINE, Processor.PAG_SEGURO().getType(), Processor.PAG_SEGURO().getUseAntiFraud());

        assertThat(result.size(), equalTo(1) );
    }

    @Test
    public void findProcessorPriceN() throws ProcessorPriceNotFoundException {

        List<ProcessorPrice> result = processorPriceService.getProcessorByPrice(masterCard,PaymentOperationType.ONLINE,Processor.CIELO().getType(), Processor.CIELO().getUseAntiFraud());

        assertThat(result.size(), equalTo(3) );

        ProcessorPrice resultRede = result.get(0);
        ProcessorPrice resultCielo = result.get(1);
        ProcessorPrice resultPagSeguro = result.get(2);

        assertThat("Valor menor", resultRede.getPrice().compareTo(processorPriceMasterRede.getPrice())==0);
        assertThat("Valor intermediario", resultCielo.getPrice().compareTo(processorPriceMasterCielo.getPrice())==0);
        assertThat("Valor maior", resultPagSeguro.getPrice().compareTo(processorPriceMasterPagseguro.getPrice())==0);
    }



}
