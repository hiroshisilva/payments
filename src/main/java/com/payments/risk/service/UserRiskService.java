package com.payments.risk.service;

import com.payments.common.model.PaymentMethod;
import com.payments.common.model.Restaurant;
import com.payments.common.model.User;
import com.payments.common.model.UserType;
import com.payments.common.repository.PaymentMethodRepository;
import com.payments.common.service.RestaurantService;
import com.payments.common.service.UserService;
import com.payments.common.exception.RestaurantNotFoundException;
import com.payments.common.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.payments.common.model.PaymentOperationType.OFFLINE;

@Service
public class UserRiskService {

    @Autowired
    private UserService userService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    public List<PaymentMethod> getPaymentMethodsByUserIdRestaurantId(Long userId, Long restaurantId) throws UserNotFoundException, RestaurantNotFoundException {

        User user = userService.getUser(userId);
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);


        return paymentMethodRepository.findPaymentMethodsByOperationTypeRestaurantId(restaurantId,user.getType().getOperationTypes());
    }

}
