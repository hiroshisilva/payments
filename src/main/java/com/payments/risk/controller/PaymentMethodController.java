package com.payments.risk.controller;


import com.payments.common.model.PaymentMethod;
import com.payments.common.exception.RestaurantNotFoundException;
import com.payments.common.exception.UserNotFoundException;
import com.payments.risk.dto.PaymentMethodDTO;
import com.payments.risk.service.UserRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PaymentMethodController {

    @Autowired
    private UserRiskService userRiskService;

        @RequestMapping(path = "/risk/payment-method/{restaurantId}/{userId}", method = RequestMethod.GET)
    public ResponseEntity getPaymentMethods(@PathVariable Long restaurantId, @PathVariable Long userId){

        try {
            List<PaymentMethod> paymentMethods = userRiskService.getPaymentMethodsByUserIdRestaurantId(userId,restaurantId);

            List<PaymentMethodDTO> paymentMethodDTOS = paymentMethods.stream().map(pm -> new PaymentMethodDTO(pm))
                                                                            .collect(Collectors.toList());

            return  ResponseEntity.ok(paymentMethodDTOS);

        } catch (UserNotFoundException | RestaurantNotFoundException e) {
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

}
