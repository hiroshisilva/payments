package com.payments.risk.dto;

import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentMethod;
import com.payments.common.model.PaymentMethodType;
import com.payments.common.model.PaymentOperationType;

import java.io.Serializable;
import java.util.Set;

public class PaymentMethodDTO implements Serializable {

    private static final long serialVersionUID = -4972088066757530744L;

    private Long id;

    private String description;

    private PaymentMethodType type;

    private PaymentOperationType operationType;

    private Set<BrandCard> brandCards;

    public PaymentMethodDTO(Long id, String description, PaymentMethodType type, PaymentOperationType operationType, Set<BrandCard> brandCards) {
        this.id = id;
        this.description = description;
        this.type = type;
        this.operationType = operationType;
        this.brandCards = brandCards;
    }

    public PaymentMethodDTO(PaymentMethod paymentMethod) {
        this(paymentMethod.getId()
                ,paymentMethod.getDescription()
                ,paymentMethod.getType()
                ,paymentMethod.getOperationType()
                ,paymentMethod.getBrandCards());
    }

    public Long getId() {
        return id;
    }

    public PaymentMethodDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public PaymentMethodDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public PaymentMethodType getType() {
        return type;
    }

    public PaymentMethodDTO setType(PaymentMethodType type) {
        this.type = type;
        return this;
    }

    public PaymentOperationType getOperationType() {
        return operationType;
    }

    public PaymentMethodDTO setOperationType(PaymentOperationType operationType) {
        this.operationType = operationType;
        return this;
    }

    public Set<BrandCard> getBrandCards() {
        return brandCards;
    }

    public PaymentMethodDTO setBrandCards(Set<BrandCard> brandCards) {
        this.brandCards = brandCards;
        return this;
    }
}
