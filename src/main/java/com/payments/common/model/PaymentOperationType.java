package com.payments.common.model;

/**
 * Created by andre on 25/01/17.
 */
public enum PaymentOperationType {
    ONLINE("ONLINE"),
    OFFLINE("OFFLINE");

    private String description;

    PaymentOperationType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
