package com.payments.common.model;


import org.hibernate.validator.constraints.NotBlank;
import javax.persistence.*;
import java.util.*;

@Entity
@Table( name = "RESTAURANT")
public class Restaurant {

    @Id
    @Column(name = "ID_RESTAURANT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(name = "NAME", length = 45)
    private String name;

    @NotBlank
    @Column(name = "DOCUMENT", length = 18, unique = true)
    private String document;

    @NotBlank
    @Column(name = "TELEPHONE", length = 45)
    private String telephone;

    @ManyToMany(targetEntity = PaymentMethod.class)
    @JoinTable(name = "RESTAURANT_PAYMENT_METHOD",
                joinColumns = @JoinColumn(name = "ID_RESTAURANT", nullable = false, referencedColumnName = "ID_RESTAURANT")
              , inverseJoinColumns = @JoinColumn(name = "ID_PAYMENT_METHOD", nullable = false, referencedColumnName = "ID_PAYMENT_METHOD"))
    private Set<PaymentMethod> paymentMethods = new HashSet<PaymentMethod>();

    public Long getId() {
        return id;
    }

    public Restaurant setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Restaurant setName(String name) {
        this.name = name;
        return this;
    }

    public String getDocument() {
        return document;
    }

    public Restaurant setDocument(String document) {
        this.document = document;
        return this;
    }

    public String getTelephone() {
        return telephone;
    }

    public Restaurant setTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public Restaurant addPaymentMethod(PaymentMethod paymentMethod){
        this.paymentMethods.add(paymentMethod);
        return this;
    }

    public Set<PaymentMethod> getPaymentMethods() {
        return Collections.unmodifiableSet(paymentMethods);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Restaurant that = (Restaurant) o;

        if (!id.equals(that.id)) return false;
        if (!name.equals(that.name)) return false;
        if (!document.equals(that.document)) return false;
        return !telephone.equals(that.telephone) ? false : true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + document.hashCode();
        result = 31 * result + telephone.hashCode();
        return result;
    }
}
