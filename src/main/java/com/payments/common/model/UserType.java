package com.payments.common.model;


import java.util.Arrays;
import java.util.List;

import static com.payments.common.model.PaymentOperationType.ONLINE;
import static com.payments.common.model.PaymentOperationType.OFFLINE;


public enum UserType {
    NEW("NEW",true,ONLINE,OFFLINE),
    RELIABLE("RELIABLE",false,ONLINE,OFFLINE),
    FRAUDSTER("FRAUDSTER",true,OFFLINE);

    private String description;

    private List<PaymentOperationType> operationTypes;

    private Boolean useAntFraud;

    UserType(String description, boolean useAntFraud, PaymentOperationType... operationTypes) {
        this.description = description;
        this.useAntFraud = useAntFraud;
        this.operationTypes = Arrays.asList(operationTypes);
    }

    public String getDescription() {
        return description;
    }

    public List<PaymentOperationType> getOperationTypes() {
        return operationTypes;
    }

    public Boolean getUseAntFraud() {
        return useAntFraud;
    }
}
