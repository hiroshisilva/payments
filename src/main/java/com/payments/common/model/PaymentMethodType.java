package com.payments.common.model;

import static com.payments.common.model.PaymentOperationType.*;

/**
 * Created by andre on 25/01/17.
 */

public enum PaymentMethodType {
    CREDIT_CARD("CREDIT_CARD"),
    DIGITAL_WALLET("DIGITAL_WALLET"),
    CASH("CASH"),
    CHECK("CHECK"),
    POS("POS");
    
    private String description;

    PaymentMethodType(String description) {
        this.description = description;
    }

    public String getDescription() { return description; }
}
