package com.payments.common.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "BRAND_CARD")
public class BrandCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_BRAND_CARD")
    private Long id;

    @NotBlank
    @Column(name = "DESCRIPTION", length = 45)
    private String description;

    public Long getId() {
        return id;
    }

    public BrandCard setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public BrandCard setDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BrandCard brandCard = (BrandCard) o;

        if (!id.equals(brandCard.id)) return false;
        return  (!description.equals(brandCard.description)) ? false : true;
    }

    @Override
    public int hashCode() {
        int result = 31 + description.hashCode();
        return result;
    }
}
