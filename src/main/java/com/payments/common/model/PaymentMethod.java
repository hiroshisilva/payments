package com.payments.common.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "PAYMENT_METHOD")
public class PaymentMethod {

    @Id
    @Column(name = "ID_PAYMENT_METHOD")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(name = "DESCRIPTION", length = 25)
    private String description;

    @NotNull
    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private PaymentMethodType type;

    @NotNull
    @Column(name = "OPERATION_TYPE")
    @Enumerated(EnumType.STRING)
    private PaymentOperationType operationType;

    @ManyToMany(cascade = {CascadeType.MERGE}, targetEntity = BrandCard.class)
    @JoinTable(name = "PAYMENT_METHODS_BRAND_CARDS"
            , joinColumns = @JoinColumn(name = "ID_PAYMENT_METHOD", nullable = false, referencedColumnName = "ID_PAYMENT_METHOD")
            ,inverseJoinColumns = @JoinColumn(name = "ID_BRAND_CARD", nullable = false, referencedColumnName = "ID_BRAND_CARD" ))
    private Set<BrandCard> brandCards = new HashSet<BrandCard>();

    @ManyToMany(mappedBy = "paymentMethods", targetEntity = Restaurant.class)
    private Set<Restaurant> restaurants = new HashSet<Restaurant>();


    public Long getId() {
        return id;
    }

    public PaymentMethod setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public PaymentMethod setDescription(String description) {
        this.description = description;
        return this;
    }

    public PaymentMethodType getType() {
        return type;
    }

    public PaymentMethod setType(PaymentMethodType type) {
        this.type = type;
        return this;
    }

    public PaymentOperationType getOperationType() {
        return operationType;
    }

    public PaymentMethod setOperationType(PaymentOperationType operationType) {
        this.operationType = operationType;
        return this;
    }

    public Set<BrandCard> getBrandCards() {
        return Collections.unmodifiableSet(brandCards);
    }

    public PaymentMethod addBrandCard(BrandCard brandCard) {
        this.brandCards.add(brandCard);
        return this;
    }

    public Set<Restaurant> getRestaurants() {
        return Collections.unmodifiableSet(restaurants);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentMethod that = (PaymentMethod) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!description.equals(that.description)) return false;
        if (type != that.type) return false;
        return operationType == that.operationType;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + description.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + operationType.hashCode();
        return result;
    }
}
