package com.payments.common.dto;

import java.io.Serializable;

public class MessageErrorDTO implements Serializable {

    private static final long serialVersionUID = -971222502433394408L;

    private Integer httpCode;
    private String message;

    public Integer getHttpCode() {
        return httpCode;
    }

    public MessageErrorDTO setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MessageErrorDTO setMessage(String message) {
        this.message = message;
        return this;
    }
}
