package com.payments.common.exception;

public class UserNotFoundException extends ValidationErrorException {
    public UserNotFoundException(Long userId) {
        super(String.format("User: %d Not Found!",userId));
    }
}
