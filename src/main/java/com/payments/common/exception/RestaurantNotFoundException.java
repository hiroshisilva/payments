package com.payments.common.exception;

public class RestaurantNotFoundException extends ValidationErrorException {

    public RestaurantNotFoundException(Long restaurantId) {
        super(String.format("Restaurant: %d Not Found!",restaurantId));
    }
}
