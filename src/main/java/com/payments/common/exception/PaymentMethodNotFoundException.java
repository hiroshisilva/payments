package com.payments.common.exception;

public class PaymentMethodNotFoundException extends ValidationErrorException {
    public PaymentMethodNotFoundException(Long paymentMethodId) {
        super(String.format("Payment Method: %d Not Found!",paymentMethodId));
    }
}
