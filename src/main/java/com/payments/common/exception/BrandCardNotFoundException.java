package com.payments.common.exception;

public class BrandCardNotFoundException extends ValidationErrorException {

    public BrandCardNotFoundException(Long brandCardId) {
        super(String.format("Brand Card: %d Not Found!",brandCardId));
    }
}
