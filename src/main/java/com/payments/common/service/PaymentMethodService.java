package com.payments.common.service;

import com.payments.common.exception.PaymentMethodNotFoundException;
import com.payments.common.model.PaymentMethod;
import com.payments.common.repository.PaymentMethodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentMethodService {

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    public PaymentMethod getPaymentMethod(Long paymentMethodId) throws PaymentMethodNotFoundException {

        PaymentMethod paymentMethod = paymentMethodRepository.findOne(paymentMethodId);

        if(paymentMethod == null){
            throw new PaymentMethodNotFoundException(paymentMethodId);
        }

        return paymentMethod;
    }

}
