package com.payments.common.service;

import com.payments.common.model.Restaurant;
import com.payments.common.repository.RestaurantRepository;
import com.payments.common.exception.RestaurantNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    public Restaurant getRestaurant(Long restaurantId) throws RestaurantNotFoundException {

        Restaurant restaurant = restaurantRepository.findOne(restaurantId);

        if(restaurant == null){
            throw new RestaurantNotFoundException(restaurantId);
        }

        return restaurant;
    }

}
