package com.payments.common.service;

import com.payments.common.model.User;
import com.payments.common.repository.UserRepository;
import com.payments.common.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User getUser(Long userId) throws UserNotFoundException {
        User user = userRepository.findOne(userId);

        if(user == null){
            throw new UserNotFoundException(userId);
        }

        return user;
    }

}
