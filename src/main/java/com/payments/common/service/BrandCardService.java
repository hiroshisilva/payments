package com.payments.common.service;

import com.payments.common.exception.BrandCardNotFoundException;
import com.payments.common.model.BrandCard;
import com.payments.common.repository.BrandCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandCardService {

    @Autowired
    private BrandCardRepository brandCardRepository;

    public BrandCard getBrandCard(Long brandCardId) throws BrandCardNotFoundException {

        BrandCard brandCard = brandCardRepository.findOne(brandCardId);

        if(brandCard == null){
            throw new BrandCardNotFoundException(brandCardId);
        }

        return  brandCard;
    }

}
