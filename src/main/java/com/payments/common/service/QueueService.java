package com.payments.common.service;

import java.io.Serializable;

public interface QueueService <T extends Serializable> {

    public void sendMessage(T message);

}
