package com.payments.common.repository;

import com.payments.common.model.BrandCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface BrandCardRepository extends JpaRepository<BrandCard, Long> {
}
