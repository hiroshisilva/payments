package com.payments.common.repository;

import com.payments.common.model.PaymentMethod;
import com.payments.common.model.PaymentOperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long> {

    public List<PaymentMethod> findByOperationType(PaymentOperationType operationType);

    @Query("SELECT DISTINCT pm " +
            " FROM PaymentMethod pm " +
            "INNER JOIN FETCH pm.restaurants rest " +
            "INNER JOIN FETCH pm.brandCards " +
            "where rest.id = ?1 AND pm.operationType IN (?2)")
    public List<PaymentMethod> findPaymentMethodsByOperationTypeRestaurantId(Long restaurntId, List<PaymentOperationType> operationType);

    @Query("SELECT DISTINCT pm FROM PaymentMethod pm " +
            "INNER JOIN FETCH pm.restaurants rest " +
            "INNER JOIN FETCH pm.brandCards " +
            "where rest.id = ?1")
    public List<PaymentMethod> findPaymentMethodsByRestaurantId(Long restaurntId);


}
