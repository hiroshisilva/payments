package com.payments.capture.controller;


import com.payments.capture.dto.TransactionDTO;
import com.payments.capture.exception.CaptorNotImplementedException;
import com.payments.capture.exception.ProcessorNotAccessibleException;
import com.payments.capture.exception.UserCanNotUseThisOperationException;
import com.payments.capture.model.Transaction;
import com.payments.capture.service.TransactionService;
import com.payments.common.dto.MessageErrorDTO;
import com.payments.common.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CaptureController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(path = "/transaction", method = RequestMethod.POST)
    public ResponseEntity processTransaction(@RequestBody TransactionDTO transactionDTO) {

        try {
            Transaction  transaction = transactionService.process(transactionDTO);

            transactionDTO
                    .setTransactionId(transaction.getId())
                    .setStatus(transaction.getStatus());

            return ResponseEntity.status(HttpStatus.OK).body(transactionDTO);

        } catch (ValidationErrorException | CaptorNotImplementedException e) {
            logger.info("Validation Error",e);

            MessageErrorDTO messageError = new MessageErrorDTO()
                    .setHttpCode(HttpStatus.BAD_REQUEST.value())
                    .setMessage(e.getMessage());

            return ResponseEntity.status(messageError.getHttpCode()).body(messageError);

        } catch (UserCanNotUseThisOperationException e){
            MessageErrorDTO messageError = new MessageErrorDTO()
                    .setHttpCode(HttpStatus.UNAUTHORIZED.value())
                    .setMessage(e.getMessage());

            return ResponseEntity.status(messageError.getHttpCode()).body(messageError);

        } catch (ProcessorNotAccessibleException e) {

            MessageErrorDTO messageError = new MessageErrorDTO()
                    .setHttpCode(HttpStatus.SERVICE_UNAVAILABLE.value())
                    .setMessage(e.getMessage());

            return ResponseEntity.status(messageError.getHttpCode()).body(messageError);

        }

    }
}
