package com.payments.capture.exception;

import com.payments.common.exception.ValidationErrorException;

public class ProcessorPriceNotFoundException extends ValidationErrorException {

    public ProcessorPriceNotFoundException() {
        super("ProcessorPrice Not Found!");
    }
}
