package com.payments.capture.exception;

public class CaptorNotImplementedException extends Exception {
    public CaptorNotImplementedException() {
        super("Captor Not Implemented");
    }
}
