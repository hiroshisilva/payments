package com.payments.capture.exception;

public class UserCanNotUseThisOperationException extends Exception {

    public UserCanNotUseThisOperationException() {
        super("User can not use this operation");
    }

    public UserCanNotUseThisOperationException(String message) {
        super(message);
    }

    public UserCanNotUseThisOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserCanNotUseThisOperationException(Throwable cause) {
        super("User can not use this operation",cause);
    }

    public UserCanNotUseThisOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
