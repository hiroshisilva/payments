package com.payments.capture.exception;

public class ProcessorNotAccessibleException extends Exception{
    public ProcessorNotAccessibleException() {
        super("Processor can not accessible!");
    }

    public ProcessorNotAccessibleException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProcessorNotAccessibleException(Throwable cause) {
        super("Processor can not accessible!",cause);
    }

    public ProcessorNotAccessibleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ProcessorNotAccessibleException(String message) {
        super(message);
    }
}
