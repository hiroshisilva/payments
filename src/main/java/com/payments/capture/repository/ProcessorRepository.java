package com.payments.capture.repository;

import com.payments.capture.model.Processor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessorRepository extends JpaRepository<Processor, Integer> {
}
