package com.payments.capture.repository;

import com.payments.capture.model.ProcessorPrice;
import com.payments.common.model.PaymentMethodType;
import com.payments.common.model.PaymentOperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProcessorPriceRepository extends JpaRepository<ProcessorPrice,Long> {


    @Query("  SELECT PP FROM ProcessorPrice PP " +
            " INNER JOIN FETCH PP.brandCard BC" +
            " INNER JOIN FETCH PP.processor P" +
            " WHERE BC.id = ?1 AND PP.operationType = ?2 AND P.type = ?3 AND P.useAntiFraud = ?4" +
            " ORDER BY PP.price ASC")
    public List<ProcessorPrice> getProcessorPriceByBrandCardTypeOperation(Long brandCardId, PaymentOperationType operationType, PaymentMethodType paymentMethodType, Boolean hasAntiFraud);

    @Query("  SELECT PP FROM ProcessorPrice PP " +
            " INNER JOIN FETCH PP.brandCard BC" +
            " INNER JOIN FETCH PP.processor P" +
            " WHERE BC.id = ?1 AND PP.operationType = ?2 AND P.type = ?3" +
            " ORDER BY PP.price ASC")
    public List<ProcessorPrice> getProcessorPriceByBrandCardTypeOperation(Long brandCardId, PaymentOperationType operationType, PaymentMethodType paymentMethodType);
}
