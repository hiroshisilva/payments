package com.payments.capture.model;

import com.payments.common.model.PaymentMethodType;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PROCESSOR")
public class  Processor {

    public static Processor REDE(){return new Processor(1,"Rede", PaymentMethodType.CREDIT_CARD, false, Captor.MAXIPAGO);}
    public static Processor CIELO(){return new Processor(2,"CIELO", PaymentMethodType.CREDIT_CARD, false, Captor.SOFTWARE_EXPRESS);}
    public static Processor PAG_SEGURO(){return new Processor(3,"PAG_SEGURO", PaymentMethodType.CREDIT_CARD, true, Captor.PAG_SEGURO);}
    public static Processor VISA_CHECKOUT(){return new Processor(4,"VISA_CHECKOUT", PaymentMethodType.DIGITAL_WALLET,true, Captor.VISA_CHECKOUT);}

    private Processor(Integer id, String name, PaymentMethodType type, Boolean useAntiFraud,Captor captor) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.useAntiFraud = useAntiFraud;
        this.captor = captor;
    }

    private Processor() {
    }

    @Id
    @Column(name = "ID_PROCESSOR")
    private Integer id;

    @NotBlank
    @Column(name = "NAME")
    private String name;

    @NotNull
    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private PaymentMethodType type;

    @NotNull
    @Column(name = "USE_ANTIFRAUD")
    private Boolean useAntiFraud;

    @NotNull
    @Column(name = "CAPTOR")
    @Enumerated(EnumType.STRING)
    private Captor captor;

    public Integer getId() {
        return id;
    }

    public Processor setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Processor setName(String name) {
        this.name = name;
        return this;
    }

    public PaymentMethodType getType() {
        return type;
    }

    public Processor setType(PaymentMethodType type) {
        this.type = type;
        return this;
    }

    public Boolean getUseAntiFraud() {
        return useAntiFraud;
    }

    public Processor setUseAntiFraud(Boolean useAntiFraud) {
        this.useAntiFraud = useAntiFraud;
        return this;
    }

    public Captor getCaptor() {
        return captor;
    }

    public Processor setCaptor(Captor captor) {
        this.captor = captor;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Processor processor = (Processor) o;

        if (id != null ? !id.equals(processor.id) : processor.id != null) return false;
        if (name != null ? !name.equals(processor.name) : processor.name != null) return false;
        if (type != processor.type) return false;
        if (useAntiFraud != null ? !useAntiFraud.equals(processor.useAntiFraud) : processor.useAntiFraud != null)
            return false;
        return captor == processor.captor;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (useAntiFraud != null ? useAntiFraud.hashCode() : 0);
        result = 31 * result + (captor != null ? captor.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Processor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", useAntiFraud=" + useAntiFraud +
                ", captor=" + captor +
                '}';
    }
}
