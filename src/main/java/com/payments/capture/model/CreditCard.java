package com.payments.capture.model;

import com.payments.common.model.BrandCard;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class CreditCard {

    @CreditCardNumber
    @Column(name = "CREDIT_CARD")
    private String number;

    @ManyToOne
    @JoinColumn(name = "ID_BRAND_CARD", referencedColumnName = "ID_BRAND_CARD")
    private BrandCard brandCard;

    private String cvv;

    public String getNumber() {
        return number;
    }

    public CreditCard setNumber(String number) {
        this.number = number;
        return this;
    }

    public BrandCard getBrandCard() {
        return brandCard;
    }

    public CreditCard setBrandCard(BrandCard brandCard) {
        this.brandCard = brandCard;
        return this;
    }

    public String getCvv() {
        return cvv;
    }

    public CreditCard setCvv(String cvv) {
        this.cvv = cvv;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditCard that = (CreditCard) o;

        if (number != null ? !number.equals(that.number) : that.number != null) return false;
        if (brandCard != null ? !brandCard.equals(that.brandCard) : that.brandCard != null) return false;
        return cvv != null ? cvv.equals(that.cvv) : that.cvv == null;
    }

    @Override
    public int hashCode() {
        int result = number != null ? number.hashCode() : 0;
        result = 31 * result + (brandCard != null ? brandCard.hashCode() : 0);
        result = 31 * result + (cvv != null ? cvv.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "number='" + number + '\'' +
                ", brandCard=" + brandCard +
                ", cvv='" + cvv + '\'' +
                '}';
    }
}
