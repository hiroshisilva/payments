package com.payments.capture.model;


import com.payments.common.model.Restaurant;
import com.payments.common.model.User;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "TRANSACTION")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_TRANSACTION")
    private Long id;

    @NotNull
    @Column(name = "VALUE")
    private BigDecimal value;

    @Embedded
    private CreditCard creditCard;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "ID_TRANSACTION_STATUS")
    private TransactionStatus status;

    @NotNull
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "ID_RESTAURANT", referencedColumnName = "ID_RESTAURANT")
    private Restaurant restaurant;

    @NotNull
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER")
    private User user;

    @NotNull
    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    private Country country;

    @ManyToOne
    @JoinColumn(name = "ID_PROCESSOR", referencedColumnName = "ID_PROCESSOR")
    private Processor processor;

    public Long getId() {
        return id;
    }

    public Transaction setId(Long id) {
        this.id = id;
        return this;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Transaction setValue(BigDecimal value) {
        this.value = value;
        return this;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public Transaction setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        return this;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public Transaction setStatus(TransactionStatus status) {
        this.status = status;
        return this;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public Transaction setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Transaction setUser(User user) {
        this.user = user;
        return this;
    }

    public Country getCountry() {
        return country;
    }

    public Transaction setCountry(Country country) {
        this.country = country;
        return this;
    }

    public Processor getProcessor() {
        return processor;
    }

    public Transaction setProcessor(Processor processor) {
        this.processor = processor;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (creditCard != null ? !creditCard.equals(that.creditCard) : that.creditCard != null) return false;
        if (status != that.status) return false;
        if (restaurant != null ? !restaurant.equals(that.restaurant) : that.restaurant != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (country != that.country) return false;
        return processor != null ? processor.equals(that.processor) : that.processor == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (creditCard != null ? creditCard.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (restaurant != null ? restaurant.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (processor != null ? processor.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", value=" + value +
                ", creditCard=" + creditCard +
                ", status=" + status +
                ", restaurant=" + restaurant +
                ", user=" + user +
                ", country=" + country +
                ", processor=" + processor +
                '}';
    }
}
