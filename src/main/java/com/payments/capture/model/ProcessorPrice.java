package com.payments.capture.model;

import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentOperationType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Generated;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "PROCESSOR_PRICE")
public class ProcessorPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_PROCESSOR_PRICE")
    private Long id;

    @NotNull
    @Column(name = "PRICE")
    private BigDecimal price;

    @NotNull
    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "ID_BRAND_CARD", referencedColumnName = "ID_BRAND_CARD")
    private BrandCard brandCard;

    @NotNull
    @Column(name = "OPERATION_TYPE")
    @Enumerated(EnumType.STRING)
    private PaymentOperationType operationType;

    @ManyToOne
    @JoinColumn(name = "ID_PROCESSOR")
    private Processor processor;

    public Long getId() {
        return id;
    }

    public ProcessorPrice setId(Long id) {
        this.id = id;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public ProcessorPrice setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public BrandCard getBrandCard() {
        return brandCard;
    }

    public ProcessorPrice setBrandCard(BrandCard brandCard) {
        this.brandCard = brandCard;
        return this;
    }

    public PaymentOperationType getOperationType() {
        return operationType;
    }

    public ProcessorPrice setOperationType(PaymentOperationType operationType) {
        this.operationType = operationType;
        return this;
    }

    public Processor getProcessor() {
        return processor;
    }

    public ProcessorPrice setProcessor(Processor processor) {
        this.processor = processor;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessorPrice that = (ProcessorPrice) o;

        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (brandCard != null ? !brandCard.equals(that.brandCard) : that.brandCard != null) return false;
        return operationType == that.operationType;
    }

    @Override
    public int hashCode() {
        int result = price != null ? price.hashCode() : 0;
        result = 31 * result + (brandCard != null ? brandCard.hashCode() : 0);
        result = 31 * result + (operationType != null ? operationType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProcessorPrice{" +
                "id=" + id +
                ", price=" + price +
                ", brandCard=" + brandCard +
                ", operationType=" + operationType +
                ", processor=" + processor +
                '}';
    }
}
