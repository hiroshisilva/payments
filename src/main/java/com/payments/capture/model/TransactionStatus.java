package com.payments.capture.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTION_STATUS")
public enum TransactionStatus {
    IN_ANALYSI(1,"IN_ANALYSI"),
    DISAPPROVED(2,"DISAPPROVED"),
    APPROVED(3,"APPROVED");

    @Id
    @Column(name = "ID_TRANSACTION_STATUS")
    private Integer id;

    @Column(name = "DESCRIPTION")
    private String description;

    private TransactionStatus(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }


}
