package com.payments.capture.model;

public enum Country {

    BR("Brazil"),MX("Mexico"),CO("Colombia"),AR("Argentina");

    private String contryName;

    Country(String contryCode) {
        this.contryName = contryCode;
    }

}
