package com.payments.capture.dto;

import com.payments.capture.model.Country;
import com.payments.capture.model.TransactionStatus;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransactionDTO implements Serializable{

    private static final long serialVersionUID = -2067229174475458433L;

    private Long transactionId;

    private Long brandCardId;

    private Country country;

    private Long paymentMethodId;

    private Long userId;

    private Long restaurantId;

    private String cardNumber;

    private String cvv;

    private BigDecimal value;

    private TransactionStatus status;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public TransactionDTO setTransactionId(Long transactionId) {
        this.transactionId = transactionId;

        return this;
    }

    public Long getBrandCardId() {
        return brandCardId;
    }

    public TransactionDTO setBrandCardId(Long brandCardId) {
        this.brandCardId = brandCardId;

        return this;
    }

    public Country getCountry() {
        return country;
    }

    public TransactionDTO setCountry(Country country) {
        this.country = country;

        return this;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public TransactionDTO setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;

        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public TransactionDTO setUserId(Long userId) {
        this.userId = userId;

        return this;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public TransactionDTO setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;

        return this;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public TransactionDTO setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;

        return this;
    }

    public String getCvv() {
        return cvv;
    }

    public TransactionDTO setCvv(String cvv) {
        this.cvv = cvv;

        return this;
    }

    public BigDecimal getValue() {
        return value;
    }

    public TransactionDTO setValue(BigDecimal value) {
        this.value = value;

        return this;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public TransactionDTO setStatus(TransactionStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionDTO that = (TransactionDTO) o;

        if (transactionId != null ? !transactionId.equals(that.transactionId) : that.transactionId != null)
            return false;
        if (brandCardId != null ? !brandCardId.equals(that.brandCardId) : that.brandCardId != null) return false;
        if (country != that.country) return false;
        if (paymentMethodId != null ? !paymentMethodId.equals(that.paymentMethodId) : that.paymentMethodId != null)
            return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (restaurantId != null ? !restaurantId.equals(that.restaurantId) : that.restaurantId != null) return false;
        if (cardNumber != null ? !cardNumber.equals(that.cardNumber) : that.cardNumber != null) return false;
        if (cvv != null ? !cvv.equals(that.cvv) : that.cvv != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        return status == that.status;
    }

    @Override
    public int hashCode() {
        int result = transactionId != null ? transactionId.hashCode() : 0;
        result = 31 * result + (brandCardId != null ? brandCardId.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (paymentMethodId != null ? paymentMethodId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (restaurantId != null ? restaurantId.hashCode() : 0);
        result = 31 * result + (cardNumber != null ? cardNumber.hashCode() : 0);
        result = 31 * result + (cvv != null ? cvv.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "transactionId=" + transactionId +
                ", brandCardId=" + brandCardId +
                ", country=" + country +
                ", paymentMethodId=" + paymentMethodId +
                ", userId=" + userId +
                ", restaurantId=" + restaurantId +
                ", cardNumber='" + cardNumber + '\'' +
                ", cvv='" + cvv + '\'' +
                ", value=" + value +
                ", status=" + status +
                '}';
    }
}
