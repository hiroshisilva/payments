package com.payments.capture.captor;

import com.payments.capture.exception.ProcessorNotAccessibleException;
import com.payments.capture.exception.UnauthorizedTransactionException;
import com.payments.capture.model.Processor;
import com.payments.capture.model.Transaction;
import com.payments.capture.model.TransactionStatus;
import com.payments.capture.repository.TransactionRepository;
import com.payments.mock.dto.TransactionMockDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class MaxiPagoCaptor extends Captor {

    private static final String REDE_URL = "http://localhost:8080/mock/maxipago/transaction";

    private RestTemplate restTemplate;

    MaxiPagoCaptor(TransactionRepository transactionRepository, Processor processor, RestTemplate restTemplate) {
        super(transactionRepository, processor);

        this.restTemplate = restTemplate;
    }

    @Override
    protected Transaction capture(Transaction transaction) throws ProcessorNotAccessibleException, UnauthorizedTransactionException {

        TransactionMockDTO transactionMockDTO =
                new TransactionMockDTO()
                        .setCredCard(transaction.getCreditCard().getNumber())
                        .setCvv(transaction.getCreditCard().getCvv())
                        .setValue(transaction.getValue());

        HttpEntity<TransactionMockDTO> request = new HttpEntity<TransactionMockDTO>(transactionMockDTO);

        ResponseEntity<TransactionMockDTO> response = this.restTemplate.postForEntity(REDE_URL,request,TransactionMockDTO.class);

        if(response.getStatusCode() == HttpStatus.SERVICE_UNAVAILABLE){
            throw new ProcessorNotAccessibleException();
        }

        if(response.getStatusCode() == HttpStatus.UNAUTHORIZED){
            throw new UnauthorizedTransactionException();
        }

        transaction.setStatus(TransactionStatus.APPROVED);

        return transaction;

    }

}
