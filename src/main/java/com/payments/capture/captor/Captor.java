package com.payments.capture.captor;

import com.payments.capture.exception.ProcessorNotAccessibleException;
import com.payments.capture.exception.UnauthorizedTransactionException;
import com.payments.capture.model.Processor;
import com.payments.capture.model.Transaction;
import com.payments.capture.model.TransactionStatus;
import com.payments.capture.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;

public abstract class Captor {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private TransactionRepository transactionRepository;
    private Processor processor;

    protected Captor(TransactionRepository transactionRepository, Processor processor) {
        this.transactionRepository = transactionRepository;
        this.processor = processor;
    }

    public Transaction process(Transaction transaction) throws ProcessorNotAccessibleException {
        try{
            transaction = capture(transaction);
        }
        catch (UnauthorizedTransactionException e) {
            logger.debug("Transaction DISAPROVED");

            transaction.setStatus(TransactionStatus.DISAPPROVED);
        }
        catch (RestClientException ex){
            logger.error("Error to send transaction %s , Exception:",processor.getName(),ex);
            throw new ProcessorNotAccessibleException(ex);
        }

        transaction.setProcessor(this.processor);

        transactionRepository.save(transaction);

        return transaction;
    }


    protected abstract Transaction capture(Transaction transaction) throws ProcessorNotAccessibleException, UnauthorizedTransactionException;

}
