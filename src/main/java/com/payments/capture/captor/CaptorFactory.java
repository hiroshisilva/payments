package com.payments.capture.captor;

import com.payments.capture.captor.*;
import com.payments.capture.exception.CaptorNotImplementedException;
import com.payments.capture.model.Processor;
import com.payments.capture.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Component
public class CaptorFactory {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private RestTemplate restTemplate;

    public Captor getCaptureService(Processor processor) throws CaptorNotImplementedException {

        com.payments.capture.model.Captor captor = processor.getCaptor();

        switch (captor){
            case MAXIPAGO:
                return new MaxiPagoCaptor(transactionRepository,processor,restTemplate);

            case PAG_SEGURO:
                return new PagseguroCaptor(transactionRepository,processor,restTemplate);

            case VISA_CHECKOUT:
                return new VisaCheckoutCaptor(transactionRepository,processor,restTemplate);

            case SOFTWARE_EXPRESS:
                return new SoftwareExpressCaptor(transactionRepository,processor,restTemplate);

            default:
                throw new CaptorNotImplementedException();
        }

    }

}
