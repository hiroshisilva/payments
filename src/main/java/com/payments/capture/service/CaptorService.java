package com.payments.capture.service;


import com.payments.capture.captor.Captor;
import com.payments.capture.captor.CaptorFactory;
import com.payments.capture.exception.CaptorNotImplementedException;
import com.payments.capture.exception.ProcessorNotAccessibleException;
import com.payments.capture.model.Processor;
import com.payments.capture.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaptorService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private CaptorFactory captorFactory;

    /***
     *
     * Processa a Transação no capturador.
     *
     * A transação é processada na primeira processadora da lista passada como parâmetro.
     *
     * Caso aconteça um erro de conexão no processamento, será executado o próximo da lista consecutivamente,
     * até uma transação seja aprovada, ou acabe a lista de processadoras.
     *
     * @param processors
     * @return
     */
    public Transaction sendTransaction(List<Processor> processors, Transaction transaction) throws CaptorNotImplementedException, ProcessorNotAccessibleException {

        for (Processor p : processors){
            try {
                Captor captor = captorFactory.getCaptureService(p);

                return captor.process(transaction);
            }
            catch (ProcessorNotAccessibleException ex) {
                logger.error("Processor %s",p,ex);
            }

        }

        throw new ProcessorNotAccessibleException();
    }

}
