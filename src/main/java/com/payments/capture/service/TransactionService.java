package com.payments.capture.service;


import com.payments.capture.dto.TransactionDTO;
import com.payments.capture.exception.CaptorNotImplementedException;
import com.payments.capture.exception.ProcessorNotAccessibleException;
import com.payments.capture.exception.ProcessorPriceNotFoundException;
import com.payments.capture.exception.UserCanNotUseThisOperationException;
import com.payments.capture.model.*;
import com.payments.common.exception.BrandCardNotFoundException;
import com.payments.common.exception.PaymentMethodNotFoundException;
import com.payments.common.exception.RestaurantNotFoundException;
import com.payments.common.exception.UserNotFoundException;
import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentMethod;
import com.payments.common.model.Restaurant;
import com.payments.common.model.User;
import com.payments.common.service.BrandCardService;
import com.payments.common.service.PaymentMethodService;
import com.payments.common.service.RestaurantService;
import com.payments.common.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {


    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BrandCardService brandCardService;

    @Autowired
    private UserService userService;

    @Autowired
    private PaymentMethodService paymentMethodService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ProcessorPriceService processorPriceService;

    @Autowired
    private CaptorService captorService;


    public Transaction process(TransactionDTO transactionDTO) throws BrandCardNotFoundException, UserNotFoundException, RestaurantNotFoundException, UserCanNotUseThisOperationException, PaymentMethodNotFoundException, ProcessorPriceNotFoundException, ProcessorNotAccessibleException, CaptorNotImplementedException {

        logger.debug("Processing transaction: %s",transactionDTO);

        Transaction transaction = this.validTransaction(transactionDTO);

        logger.debug("Transaction validated:", transaction);

        PaymentMethod paymentMethod = paymentMethodService.getPaymentMethod(transactionDTO.getPaymentMethodId());

        if( !transaction.getUser().getType().getOperationTypes().contains(paymentMethod.getOperationType())){
            throw new UserCanNotUseThisOperationException();
        }


        List<ProcessorPrice> processorPrices = processorPriceService.getProcessorByPrice
                (transaction.getCreditCard().getBrandCard()
                ,paymentMethod.getOperationType()
                ,paymentMethod.getType()
                ,transaction.getUser().getType().getUseAntFraud());

        List<Processor> processors = processorPrices.stream().map(ProcessorPrice::getProcessor).collect(Collectors.toList());

        transaction = captorService.sendTransaction(processors,transaction);

        return transaction;
    }


    private Transaction validTransaction(TransactionDTO transactionDTO) throws UserNotFoundException, BrandCardNotFoundException, RestaurantNotFoundException, PaymentMethodNotFoundException, UserCanNotUseThisOperationException {
        logger.debug("Valid Transaction: ",transactionDTO);

        User user = userService.getUser(transactionDTO.getUserId());
        BrandCard brandCard = brandCardService.getBrandCard(transactionDTO.getBrandCardId());
        Restaurant restaurant = restaurantService.getRestaurant(transactionDTO.getRestaurantId());

        Transaction transaction = new Transaction()
                .setRestaurant(restaurant)
                .setStatus(TransactionStatus.IN_ANALYSI)
                .setUser(user)
                .setCountry(transactionDTO.getCountry())
                .setValue(transactionDTO.getValue())
                .setCreditCard(new CreditCard()
                        .setBrandCard(brandCard)
                        .setCvv(transactionDTO.getCvv())
                        .setNumber(transactionDTO.getCardNumber()));


        return transaction;
    }


}
