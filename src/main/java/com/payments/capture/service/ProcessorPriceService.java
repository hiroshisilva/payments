package com.payments.capture.service;


import com.payments.capture.exception.ProcessorPriceNotFoundException;
import com.payments.capture.model.ProcessorPrice;
import com.payments.capture.repository.ProcessorPriceRepository;
import com.payments.common.model.BrandCard;
import com.payments.common.model.PaymentMethodType;
import com.payments.common.model.PaymentOperationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessorPriceService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProcessorPriceRepository processorPriceRepository;


    public List<ProcessorPrice> getProcessorByPrice(BrandCard brandCard, PaymentOperationType operationType, PaymentMethodType paymentMethodType,Boolean useAntiFraud) throws ProcessorPriceNotFoundException {

        List<ProcessorPrice> processorPrices = null;

        if (useAntiFraud){
            processorPrices = processorPriceRepository.getProcessorPriceByBrandCardTypeOperation(brandCard.getId(), operationType, paymentMethodType, useAntiFraud);
        }else{
            processorPrices = processorPriceRepository.getProcessorPriceByBrandCardTypeOperation(brandCard.getId(), operationType, paymentMethodType);
        }


        if(processorPrices.size() == 0){
            throw new ProcessorPriceNotFoundException();
        }

        return processorPrices;
    }

}
