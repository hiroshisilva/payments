package com.payments.mock.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransactionMockDTO implements Serializable{

    private static final long serialVersionUID = 6370394056060762586L;

    private BigDecimal value;

    private String credCard;

    private String cvv;

    public BigDecimal getValue() {
        return value;
    }

    public TransactionMockDTO setValue(BigDecimal value) {
        this.value = value;
        return this;
    }

    public String getCredCard() {
        return credCard;
    }

    public TransactionMockDTO setCredCard(String credCard) {
        this.credCard = credCard;
        return this;
    }

    public String getCvv() {
        return cvv;
    }

    public TransactionMockDTO setCvv(String cvv) {
        this.cvv = cvv;
        return this;
    }
}
