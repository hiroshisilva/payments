package com.payments.mock.controller;

import com.payments.mock.dto.TransactionMockDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProcessorMockController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static boolean pagSeguroIsActive = true;
    private static boolean softwareExpressIsActive = true;
    private static boolean maxiPagoIsActive = true;
    private static boolean visaCheckoutIsActive = true;


    @RequestMapping(path = "/mock/pagseguro/transaction", method = RequestMethod.POST)
    public ResponseEntity processPagseguro(TransactionMockDTO transactionMockDTO){

        if (pagSeguroIsActive){
            logger.info("Pagseguro Aproved");
            return ResponseEntity.ok().build();
        }

        logger.info("Pagseguro Service Unavailable");
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    @RequestMapping(path = "/mock/pagseguro/status/{status}", method = RequestMethod.GET)
    public ResponseEntity alterStatusPagseguro(@PathVariable Integer status){

        pagSeguroIsActive = status == 1;
        logger.info("Pagseguro active: "+pagSeguroIsActive);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/mock/softwareexpress/transaction", method = RequestMethod.POST)
    public ResponseEntity processSoftwareExpress(TransactionMockDTO transactionMockDTO){

        if (softwareExpressIsActive){
            logger.info("Software Express Aproved");
            return ResponseEntity.ok().build();
        }

        logger.info("Software Express Service Unavailable");
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    @RequestMapping(path = "/mock/softwareexpress/status/{status}", method = RequestMethod.GET)
    public ResponseEntity alterStatusSoftwareExpress(@PathVariable Integer status){

        softwareExpressIsActive = status == 1;
        logger.info("Software Express active:"+ softwareExpressIsActive);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/mock/maxipago/transaction", method = RequestMethod.POST)
    public ResponseEntity processMaxiPago(TransactionMockDTO transactionMockDTO){

        if (maxiPagoIsActive){
            logger.info("MaxiPago Aproved");
            return ResponseEntity.ok().build();
        }

        logger.info("MaxiPago Reproved");
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    @RequestMapping(path = "/mock/maxipago/status/{status}", method = RequestMethod.GET)
    public ResponseEntity alterStatusMaxiPago(@PathVariable Integer status){

        maxiPagoIsActive = status == 1;
        logger.info("MaxiPago active:"+ maxiPagoIsActive);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/mock/visacheckout/transaction", method = RequestMethod.POST)
    public ResponseEntity processVisaCheckout(TransactionMockDTO transactionMockDTO){

        if (visaCheckoutIsActive){
            logger.info("VisaCheckout Aproved");
            return ResponseEntity.ok().build();
        }

        logger.info("VisaCheckout Reproved");
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
    }

    @RequestMapping(path = "/mock/visacheckout/status/{status}", method = RequestMethod.GET)
    public ResponseEntity alterStatusVisaCheckout(@PathVariable Integer status){

        visaCheckoutIsActive = status == 1;
        logger.info("VisaCheckout active:"+visaCheckoutIsActive);

        return ResponseEntity.ok().build();
    }

}
