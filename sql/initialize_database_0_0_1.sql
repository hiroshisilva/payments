CREATE DATABASE  IF NOT EXISTS `demo-payments` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `demo-payments`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `brand_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand_card` (
  `id_brand_card` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`id_brand_card`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `brand_card` WRITE;
/*!40000 ALTER TABLE `brand_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand_card` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_method` (
  `id_payment_method` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(25) NOT NULL,
  `operation_type` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_payment_method`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `payment_method` WRITE;
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `payment_methods_brand_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods_brand_cards` (
  `id_payment_method` bigint(20) NOT NULL,
  `id_brand_card` bigint(20) NOT NULL,
  PRIMARY KEY (`id_payment_method`,`id_brand_card`),
  KEY `FK_payment_methods_brand_cards_brand_cards` (`id_brand_card`),
  CONSTRAINT `FK_payment_methods_brand_cards_payment_method` FOREIGN KEY (`id_payment_method`) REFERENCES `payment_method` (`id_payment_method`),
  CONSTRAINT `FK_payment_methods_brand_cards_brand_cards` FOREIGN KEY (`id_brand_card`) REFERENCES `brand_card` (`id_brand_card`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `payment_methods_brand_cards` WRITE;
/*!40000 ALTER TABLE `payment_methods_brand_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_methods_brand_cards` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `processor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processor` (
  `id_processor` int(11) NOT NULL,
  `captor` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `use_antifraud` bit(1) NOT NULL,
  PRIMARY KEY (`id_processor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `processor` WRITE;
/*!40000 ALTER TABLE `processor` DISABLE KEYS */;
/*!40000 ALTER TABLE `processor` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `processor_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processor_price` (
  `id_processor_price` bigint(20) NOT NULL AUTO_INCREMENT,
  `operation_type` varchar(255) NOT NULL,
  `price` decimal(19,2) NOT NULL,
  `id_brand_card` bigint(20) NOT NULL,
  `id_processor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_processor_price`),
  KEY `fk_processor_price_brand_card` (`id_brand_card`),
  KEY `fk_processor_price_processor` (`id_processor`),
  CONSTRAINT `fk_processor_price_processor` FOREIGN KEY (`id_processor`) REFERENCES `processor` (`id_processor`),
  CONSTRAINT `fk_processor_price_brand_card` FOREIGN KEY (`id_brand_card`) REFERENCES `brand_card` (`id_brand_card`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `processor_price` WRITE;
/*!40000 ALTER TABLE `processor_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `processor_price` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant` (
  `id_restaurant` bigint(20) NOT NULL AUTO_INCREMENT,
  `document` varchar(18) NOT NULL,
  `name` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  PRIMARY KEY (`id_restaurant`),
  UNIQUE KEY `uk_restaurant_document` (`document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `restaurant_payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_payment_method` (
  `id_restaurant` bigint(20) NOT NULL,
  `id_payment_method` bigint(20) NOT NULL,
  PRIMARY KEY (`id_restaurant`,`id_payment_method`),
  KEY `fk_restaurant_payment_method_payment_method` (`id_payment_method`),
  CONSTRAINT `fk_restaurant_payment_method_payment_method` FOREIGN KEY (`id_payment_method`) REFERENCES `payment_method` (`id_payment_method`),
  CONSTRAINT `fk_restaurant_payment_method_restaurant` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `restaurant_payment_method` WRITE;
/*!40000 ALTER TABLE `restaurant_payment_method` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_payment_method` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id_transaction` bigint(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `cvv` varchar(255) DEFAULT NULL,
  `credit_card` varchar(255) DEFAULT NULL,
  `value` decimal(19,2) NOT NULL,
  `id_brand_card` bigint(20) DEFAULT NULL,
  `id_processor` int(11) DEFAULT NULL,
  `id_restaurant` bigint(20) NOT NULL,
  `id_transaction_status` int(11) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  PRIMARY KEY (`id_transaction`),
  KEY `fk_transaction_brand_card` (`id_brand_card`),
  KEY `fk_transaction_processor` (`id_processor`),
  KEY `fk_transaction_restaurant` (`id_restaurant`),
  KEY `fk_transaction_transaction_status` (`id_transaction_status`),
  KEY `fk_transaction_user` (`id_user`),
  CONSTRAINT `fk_transaction_restaurant` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurant` (`id_restaurant`),
  CONSTRAINT `fk_transaction_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `fk_transaction_transaction_status` FOREIGN KEY (`id_transaction_status`) REFERENCES `transaction_status` (`id_transaction_status`),
  CONSTRAINT `fk_transaction_brand_card` FOREIGN KEY (`id_brand_card`) REFERENCES `brand_card` (`id_brand_card`),
  CONSTRAINT `fk_transaction_processor` FOREIGN KEY (`id_processor`) REFERENCES `processor` (`id_processor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `transaction_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_status` (
  `id_transaction_status` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_transaction_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `transaction_status` WRITE;
/*!40000 ALTER TABLE `transaction_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_status` ENABLE KEYS */;
UNLOCK TABLES;



DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `document` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `uk_user_document` (`document`),
  UNIQUE KEY `uk_user_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
