# Overview #

Micro serviço que simula um roteador de pagamentos de otimização de custos operacionais.

# pré requisitos #
- Java (JDK-8)
- Docker
- Docker Compose
- Postman

# Arquitetura #

- O projeto foi desenvolvido utilizando SpringBoot com o banco de dados Mysql.


### Build.###
Para efetuar o Build basta digitar a linha abaixo dentro do diretório do projeto.

Linux:
`./gradlew build docker`

windowns:
`gradle.bat build docker`

### Como executar: ###

###### Pré requisitos:
* Ceritifique-se de que as portas abaixo estão disponíveis.

  * 8080 - Servidor de aplicação
  * 3306 - Mysql

###### Excutando;

* Para executar basta executar o comando abaixo dentro do diretório do projeto.

  * `docker-compose up`


* Para testar as os endpoint's do projeto basta importar o projeto de teste no postman, o mesmo se encontra dentro do diretório postman dentro do projeto.

    * API's:
      * Alterações de Status das processadoras de cartões.
        * As API's abaixo são utilizadas para habilitar ou desabilitar o status das processadoras de cartões, quando é passada o statusId 1, significa que é para fazer com que o mock aprove as transações a serem enviados para ele, qualquer outro parâmetro fará com que o mock passe a ficar fora do ar.

          * Pagseguro: `localhost:8080/mock/pagseguro/status/{statusId}`

          * Software Express:
        `localhost:8080/mock/softwareexpress/status/0`

          * Maxipago:
        `localhost:8080/mock/maxipago/status/0`

          * Visa checkout:
        `localhost:8080/mock/visacheckout/status/{statusId}`

      * Busca de metódos de pagamentos:
        * A busca de métodos de pagamentos é o projeto getPaymentMethods dentro do projeto do postman, a onde restaurantId é o código do restaurant e o userId é o código do usuário.

        * Rota:
        `localhost:8080/risk/payment-method/{restaurantId}/{userId}`

      * Envio de transações:
        * O envio de transações é feito pelo request Send Transaction dentro do projeto do postman, e utiliza os parâmetros listados abaixo:

          * brandCardId: id da bandeira cadastrada na base de dados.

          * country: código do Pais (BR,CO, MX, AR)

          *  paymentMethodId: id do método de pagamento cadastrado na base de dados

          * userId: id do usuário cadastrado na base dedados

          * restaurantId: id do restaurant cadastrado na base de dedados.

          * cardNumber: número do cartão.

          * CVV: CVV do cartão utilizado.

          * value: valor da compra (formato 0.00)


### Dados de testes:

Os dados de testes estão já estão carregados na base de dados e podem ser consultados nas seguintes tabelas:

  * user: tabela de usuários.

  * brand_card: tabela de bandeiras cadastradas.

  * payment_brand_cards: tabela de vinculo entre os métodos de pagamento e as bandeiras cadastradas.

  * restaurant: tabela de restaurantes cadastrados.

  * retaurant_payment_method: tabela de vinculo entre restaurantes e métodos de pagamento.

  * transaction: tabelas de transações processadas.

  * transaction_status: tabela com o status possíveis para uma transação.

  * processor: tabelas com os dados das processadoras de cartões.

  * processor_price: tabela com os dados de tarifas das processadoras de cartão de acordo com a bandeira.


###  Backlog  ###

* Criar o recebimento de transações de maneira assíncrona via sqs.

* Criar consumer da fila de transações.

* Criar integrações com gateways e sub-adquirentes.
